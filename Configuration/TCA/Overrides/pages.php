<?php

/*
 * This file is part of the web-tp3/yoast_cal.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */


// RTE Config (Old style)
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    'yoast_cal',
    'Configuration/PageTS/Page.tsconfig',
    'EXT:yoast_cal :: TCEMAIN.preview for cal.'
);
